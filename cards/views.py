import datetime
import logging
import trello

from dateutil import relativedelta
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.utils import timezone

from cards.models import *
from utils.metrics.generic_metric import GenericMetric


logger = logging.getLogger('views')


def sync_board(request, name):
    logger.info('Starting view `sync_board`')
    board = get_object_or_404(Board, name=name)
    response_data = {'success': True}

    try:
        board.sync()
    except trello.exceptions.ResourceUnavailable as e:
        logger.error(e)
        response_data = {'success': False, 'msg': str(e)}

    logger.info('End view `sync_board`')

    return JsonResponse(response_data, safe=False)


def sync_all_boards(request):
    for board in Board.objects.all():
        board.sync()
        board.last_trello_sync = timezone.now()
        board.save()
    return JsonResponse({'success': True}, safe=False)


def pending_cards(request, id):
    cards = Card.objects.filter(board_id=id, closed=False).exclude(
        movements__to_column__stage__in=[IN_PROGRESS, DONE]
    )
    result = []
    for card in cards:
        labels = [l.name for l in card.labels.all()]
        result.append({'title': card.title, 'description': card.description, 'labels': labels})
    return JsonResponse(result, safe=False)


def dashboard(request):
    if 'end_time' in request.GET:
        end_time = datetime.datetime.strptime(
            request.GET['end_time'], "%Y-%m-%d")
        end_time = timezone.make_aware(end_time)
    else:
        end_time = timezone.now()
    if 'start_time' in request.GET:
        start_time = datetime.datetime.strptime(
            request.GET['start_time'], "%Y-%m-%d")
        start_time = timezone.make_aware(start_time)
    else:
        start_time = end_time - timezone.timedelta(days=7)

    boards = Board.objects.all()
    if 'board' in request.GET and request.GET['board'] and request.GET['board'] != "Todos":
        board_name = request.GET['board']
        boards = boards.filter(name=board_name)


    dashboards = []
    for board in boards:
        dashboards.append(Dashboard(
            board, start_time, end_time))

    boards = Board.objects.all()

    m_dashboard = MemberDashboard(start_time, end_time)

    if 'user' in request.GET and request.GET['user'] and request.GET['user'] != "Todos":
        member_filter = Member.objects.get(name=request.GET['user'])
        member_name = request.GET['user']

    return render(request, "dashboard.html", locals())

def to_influxdb(request):
    if 'end_time' in request.GET:
        end_time = datetime.datetime.strptime(request.GET['end_time'], "%Y-%m-%d")
        end_time = timezone.make_aware(end_time)
    else:
        end_time = timezone.now()

    boards = Board.objects.all()
    if 'board' in request.GET and request.GET['board'] != "Todos":
        board_name = request.GET['board']
        boards = boards.filter(name=board_name)

    dashboards = []
    for board in boards:
        if board.last_influx_sync != None:
            start_time = board.last_influx_sync
        else:
            start_time = timezone.now()-relativedelta.relativedelta(years=2000)
        dashboards.append(Dashboard(board, start_time, end_time))

    result = []
    for dashboard in dashboards:
        result_cards = []
        for card in dashboard.all_cards():
            card_last_move = card.last_movement_until(dashboard.end_time)
            dimensions = {
                'dashboard': dashboard.board.name,
                #'title': card.title[:25],
                'stage': card_last_move.to_column.stage,
            }
            i = 0
            for member in card.member.all():
                dimensions['member'+str(i)] = member.name
                i += 1
            i = 0
            for label in card.labels.all():
                dimensions['label'+str(i)] = label.name
                i += 1

            if 'member0' not in dimensions:
                dimensions['member0'] = '-'
            if 'label0' not in dimensions:
                dimensions['label0'] = '-'
            is_it_wip = 0
            if card_last_move.to_column.name == "Doing":
                is_it_wip = 1
            is_it_done = o
            if card.is_done():
                is_it_done = 1
            values = {
                'reaction_time': card.reaction_time(end_time).total_seconds(),
                'cycle_time': card.cycle_time(end_time).total_seconds(),
                'lead_time': card.lead_time().total_seconds(),
                'is_done': is_it_done,
                'is_wip': is_it_wip
            }
            GenericMetric('kanban_metrics', dimensions, values, card_last_move.timestamp).post_data()
            result_cards.append((dimensions['dashboard'], card.title))
        dashboard.board.last_influx_sync = dashboard.end_time
        dashboard.board.save()
        result.append(result_cards)
    return JsonResponse(result, safe=False)
