from django.conf.urls import url

from cards.views import *


urlpatterns = [
    url(r'^dashboard', dashboard),
    url(r'^board/(?P<id>\d+)/pending_cards', pending_cards),
    url(r'^board/(?P<name>.+)/sync', sync_board),
    url(r'^board/sync_all/', sync_all_boards),
    url(r'^upload_metrics', to_influxdb, name="upload_metrics"),
]
