import datetime

from django.utils import timezone
from django.test import TestCase
from django.utils.dateparse import parse_datetime

from cards.models import *

DT1 = timezone.make_aware(
    datetime.datetime(2017, 1, 1, 0, 0, 0))
DT2 = timezone.make_aware(
    datetime.datetime(2017, 1, 1, 0, 1, 0))
DT3 = timezone.make_aware(
    datetime.datetime(2017, 1, 1, 0, 2, 30))


class TestBase(TestCase):

    def setUp(self):
        self.board = Board.objects.create(
            trello_id=1, name="BOARD")
        self.to_do = self.board.columns.create(
            trello_id=1, name="To Do", stage="P")
        self.doing = self.board.columns.create(
            trello_id=2, name="Doing", stage="I")
        self.done = self.board.columns.create(
            trello_id=3, name="Done", stage="D")
        self.card = self.board.cards.create(
            trello_id=1, title="Calcular metricas")


class TestMovements(TestBase):

    def setUp(self):
        super(TestMovements, self).setUp()
        self.card.movements.create(
            trello_id=1, timestamp=DT1, to_column=self.to_do)
        self.card.movements.create(
            trello_id=2, timestamp=DT2, to_column=self.doing)
        self.card.movements.create(
            trello_id=3, timestamp=DT3, to_column=self.done)

    def test_pending_time(self):
        self.assertEqual(
            self.card.time_in_column(self.to_do), (DT2 - DT1).total_seconds())

    def test_doing_time(self):
        self.assertEqual(
            self.card.time_in_column(self.doing), (DT3 - DT2).total_seconds())

    def test_reaction_time(self):
        self.assertEqual(
            self.card.reaction_time(), DT2 - DT1)

    def test_cycle_time(self):
        self.assertEqual(
            self.card.cycle_time(), DT3 - DT2)

    def test_lead_time(self):
        self.assertEqual(
            self.card.lead_time(), DT3 - DT1)


class TestDashboard(TestMovements):

    def setUp(self):
        super(TestDashboard, self).setUp()
        self.dashboard = Dashboard(self.board, DT1, DT3)

    def test_done_cards(self):
        self.assertEqual(list(self.dashboard.done_cards()), [self.card])
