# -*- coding: utf-8 -*-
import json

from django.utils import timezone
from django.core.management.base import BaseCommand
from django.conf import settings

from cards.models import *


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--board_name', dest='board_name')

    def handle(self, *args, **options):
        board_name = options['board_name']
        if board_name == 'None' or not board_name:
            for board in Board.objects.all():
                board.sync()
        else:
            board = Board.objects.get(name=board_name)
            board.sync()
