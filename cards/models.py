import datetime
import logging
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from trello import TrelloClient

logger = logging.getLogger('models')


PENDING = 'P'
IN_PROGRESS = 'I'
DONE = 'D'
STAGES = (
    (PENDING, 'Pending'),
    (IN_PROGRESS, 'In progress'),
    (DONE, 'Done'),
)


def get_trello_client():
    return TrelloClient(
        api_key=settings.TRELLO_API_KEY,
        api_secret='',
        token=settings.TRELLO_TOKEN,
        token_secret='',
    )


def average(elements, func, neutral):
    total = neutral
    for elem in elements:
        total += getattr(elem, func)()
    return total / len(elements)


class Board(models.Model):
    trello_id = models.CharField(max_length=100)
    name = models.CharField(max_length=250)
    last_trello_sync = models.DateTimeField(null=True, blank=True)
    last_influx_sync = models.DateTimeField(null=True, blank=True)

    def reaction_columns(self):
        return self.columns.filter(stage=PENDING)

    def cycle_columns(self):
        return self.columns.filter(stage=IN_PROGRESS)

    def __str__(self):
        return self.name

    def sync(self):
        logger.info('Starting synchronization of board {}'.format(self.name))
        client = get_trello_client()

        trello_board = client.get_board(self.trello_id)
        trello_columns = trello_board.all_lists()
        
        # make sure all columns are present in the db
        for trello_column in trello_columns:
            column, created = Column.objects.get_or_create(
                trello_id=trello_column.id,
                board=self,
                defaults={'name': trello_column.name}
            )
            if created:
                logger.info('Created List "{}"'.format(column.name))

        for trello_column in trello_columns:
            column = Column.objects.get(trello_id=trello_column.id, board=self)
            logger.info('List "{}"'.format(column.name))
            for trello_card in trello_column.list_cards():
                if trello_column.closed:
                    pass_column = trello_column
                else:
                    pass_column = None
                self.import_card(trello_card, client, pass_column)
        
        logger.info('Syncronizing closed cards')
        for trello_card in trello_board.closed_cards():
            try:
                card = Card.objects.get(board=self, trello_id=trello_card.id)
                card.closed = True
                if trello_card.name == 'Interfaz para reanalisis':
                    print(trello_card.date_last_activity)
                card.last_activity = trello_card.date_last_activity
                card.save()
            except ObjectDoesNotExist:
                self.import_card(trello_card, client)

        self.last_trello_sync = timezone.now()
        self.save()
        logger.info('Board {} is now synchronized'.format(self.name))


    def import_card(self, trello_card, client, trello_column=None):

        last_sync = self.last_trello_sync or timezone.make_aware(timezone.datetime.min)
        if last_sync >= trello_card.date_last_activity:
            return

        card, created = Card.objects.get_or_create(
            board=self,
            trello_id=trello_card.id,
            defaults={'title': trello_card.name}
        )

        logger.info('Card "{}" {}'.format(trello_card.name, '(NEW)' if created else ''))
        if trello_column is not None and trello_column.closed:
            card.closed = True
        else:
            card.closed = trello_card.closed
        card.last_activity = trello_card.date_last_activity
        card.title = trello_card.name
        

        if hasattr(trello_card, 'idMembers'):
            for trello_member_id in trello_card.idMembers:
                try:
                    assigned_member = Member.objects.get(trello_id=trello_member_id)
                except ObjectDoesNotExist:
                    trello_member = client.get_member(trello_member_id)
                    assigned_member, created = Member.objects.get_or_create(
                        name=trello_member.full_name
                    )
                    if not created:
                        assigned_member.trello_id = trello_member_id
                        assigned_member.save()

                card.member.add(assigned_member)

        if hasattr(trello_card, 'idLabels'):
            labels_to_remove = {l for l in card.labels.all()}
            for trello_label_id in trello_card.idLabels:

                trello_label = client.get_label(trello_label_id, self.trello_id)

                label, _ = Label.objects.get_or_create(
                    trello_id=trello_label_id,
                    defaults={'name': trello_label.name}
                )
                # update its name
                label.name = trello_label.name
                label.save()
                card.labels.add(label)
                labels_to_remove.discard(label)

            for to_remove in labels_to_remove:
                card.labels.remove(to_remove)

        movements = sorted(trello_card.list_movements(), key=lambda l: l['datetime'])

        for trello_move in movements:
            try:
                if card.movements.count() == 0:
                    # set original column as first movement
                    print (trello_move['source']['id'])
                    card.movements.create(
                        to_column=Column.objects.get(trello_id=trello_move['source']['id']),
                        timestamp=timezone.utc.localize(trello_card.card_created_date),
                    )

                card.movements.get_or_create(
                    to_column=Column.objects.get(trello_id=trello_move['destination']['id']),
                    timestamp=trello_move['datetime']
                )
            except ObjectDoesNotExist:
                pass

        # set last_sync
        if trello_column is not None:
            try:
                card.last_activity = card.movements.last().timestamp
            except AttributeError:
                pass
        card.save()



class Column(models.Model):
    trello_id = models.CharField(max_length=100)
    board = models.ForeignKey(
        Board, related_name="columns", null=True, blank=True)
    name = models.CharField(max_length=250)
    stage = models.CharField(
        max_length=1, choices=STAGES, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        # TODO: order by position in dashboard
        ordering = ['id']


class Label(models.Model):
    trello_id = models.CharField(max_length=100)
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name

class Member(models.Model):
    """
    Member class created to link persons with cards. We can, then, calculate
    stats individually for each member.
    """
    name = models.CharField(max_length=100) # name = Trello's Full Name
    trello_id = models.CharField(max_length=100, blank=True, null=True)
    last_influx_sync = models.DateTimeField(blank=True, null=True)

    def lead_time(self, start_time, end_time):
        """
        How much time has spent each member in completing their tasks?
        """
        total = 0
        cards = self.card_set.filter(
            movements__timestamp__gte=start_time,
            movements__timestamp__lte=end_time).distinct()
        for card in cards:
            total += card.lead_time().total_seconds()
        return total

    def wip(self, start_time, end_time):
        """
        How many cards is the member working on?
        """
        wips = 0
        for card in self.card_set.filter(
            movements__timestamp__gte=start_time,
            movements__timestamp__lte=end_time).distinct():
            last_movement = card.movements.filter(
                timestamp__gte=start_time,
                timestamp__lte=end_time).order_by('-timestamp')[0]
            print (last_movement)
            if last_movement.to_column.name == "Doing":
               wips += 1
        return wips

    def throughput(self, start_time, end_time):
        """
        How many works has the member finished?
        """
        works = self.card_set.filter(
            movements__timestamp__gte=start_time,
            movements__timestamp__lte=end_time,
            movements__to_column__stage=DONE).distinct() #DONE
        return len(works)

    def __str__(self):
        return self.name


class Card(models.Model):
    trello_id = models.CharField(max_length=100)
    board = models.ForeignKey(
        Board, related_name="cards", null=True, blank=True)
    title = models.TextField()
    description = models.TextField(null=True, blank=True)
    closed = models.BooleanField(default=False)
    labels = models.ManyToManyField(Label, blank=True)
    member = models.ManyToManyField(Member)

    last_activity = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.title

    def time_in_column(self, column, end_time=None):
        total = 0
        if self.closed:
            end_time = self.last_activity
        if end_time == None:
            end_time = timezone.now()
        in_column_since = None
        in_column_until = None
        for move in self.movements.filter(to_column=column, timestamp__lte=end_time):
            if in_column_since == None or in_column_since < move.timestamp:
                in_column_since = move.timestamp
        for move in self.movements.filter(timestamp__lte=end_time):
            if move.to_column != column:
                if in_column_since is not None and move.timestamp > in_column_since:
                    if in_column_until == None or in_column_until > move.timestamp:
                        in_column_until = move.timestamp

        if in_column_since is not None:
            if in_column_until is not None:
                total = (in_column_until-in_column_since).total_seconds()
            else:
                total = (end_time-in_column_since).total_seconds()
        return total

    def is_done(self):
        return self.movements.filter(
            to_column__stage__isnull=False).last().to_column.stage == DONE

    def stage_time(self, stage, end_time=None):
        result = 0
        for column in Column.objects.filter(
                movements__card=self, stage=stage).distinct():
            result += self.time_in_column(column, end_time)
        return result

    def stage_columns(self, stage, end_time=None):
        result = []
        for column in self.board.columns.filter(
                stage=stage).distinct():
            result.append(
                {'name': column.name,
                 'time': timezone.timedelta(seconds=self.time_in_column(column, end_time))})
        return result

    def reaction_time(self, end_time=None):
        return timezone.timedelta(seconds=self.stage_time(PENDING, end_time))

    def cycle_time(self, end_time=None):
        return timezone.timedelta(seconds=self.stage_time(IN_PROGRESS, end_time))

    def reaction_columns(self, end_time=None):
        return self.stage_columns(PENDING, end_time)

    def cycle_columns(self, end_time=None):
        return self.stage_columns(IN_PROGRESS, end_time)

    def lead_time(self, end_time=None):
        if self.is_done():
            return self.reaction_time(end_time) + self.cycle_time(end_time)
        else:
            return timezone.timedelta(0)

    def last_movement_until(self, end_time=None):
        if end_time == None:
            return self.movements.last()
        else:
            return  self.movements.filter(timestamp__lte=end_time).last()


class Movement(models.Model):
    trello_id = models.CharField(max_length=100, null=True, blank=True)
    timestamp = models.DateTimeField()
    card = models.ForeignKey(Card, related_name="movements")
    to_column = models.ForeignKey(Column, related_name="movements")

    def __str__(self):
        return "{} {}".format(self.timestamp, self.to_column)

    class Meta:
        ordering = ['timestamp']


class Dashboard(object):

    def __init__(self, board, start_time, end_time):
        self.board = board
        self.start_time = start_time
        self.end_time = end_time

    def done_cards(self):
        return self.board.cards.filter(
            movements__timestamp__gte=self.start_time,
            movements__timestamp__lte=self.end_time,
            movements__to_column__stage=DONE,
        ).distinct()

    def all_cards(self):
        """
        Returns every card which is pending, in process or done.
        """
        return self.board.cards.filter(
            movements__timestamp__gte=self.start_time,
            movements__timestamp__lte=self.end_time,
            movements__to_column__stage__isnull=False).distinct()

    def all_cards_with_columns(self):
        cards = []
        for card in self.board.cards.filter(
            movements__timestamp__gte=self.start_time,
            movements__timestamp__lte=self.end_time,
            movements__to_column__stage__isnull=False).distinct():
            cards.append((card, card.reaction_columns(self.end_time), card.cycle_columns(self.end_time)))
        return cards

    def throughput(self):
        return self.done_cards().count()

    def lead_time(self):
        if self.throughput():
            return average(
                self.done_cards(), 'lead_time', timezone.timedelta(0))
        else:
            return 0

    def cycle_time(self):
        try:
            return average(
                self.all_cards(), 'cycle_time', timezone.timedelta(0))
        except ZeroDivisionError:
            return 0

    def reaction_time(self):
        try:
            return average(
                self.all_cards(), 'reaction_time', timezone.timedelta(0))
        except ZeroDivisionError:
            return 0

class MemberDashboard(models.Model):
    """
    A little secondary dashboard containing members' name, throughput, wip and lead time,
    for members who are appointed to cards between start_time and end_time.
    """
    def __init__(self, start_time, end_time):
        self.start_time = start_time
        self.end_time = end_time

    def get_members(self):
        """
        Returns appointed members for cards between start_time and end_time.
        """
        members = Member.objects.filter(card__isnull=False).distinct()
        members_send = []
        for member in members:
            members_dict = {
                'member': member,
                'throughput': member.throughput(self.start_time, self.end_time),
                'wip': member.wip(self.start_time, self.end_time),
                'lead_time': member.lead_time(self.start_time, self.end_time)
            }
            members_send.append(members_dict)
        return members_send
