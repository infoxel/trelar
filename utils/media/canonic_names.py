"""Get canonic names for a media given its mediadelivery_id.

This script performs GET requests to a server to get the name.

Information is cached in a sqlite db on a per process file.

Cache is invalidated every N hours.
"""

import shelve
import requests
import logging
from datetime import datetime
from datetime import timedelta


requests_exceptions = (
    requests.exceptions.ConnectionError,  # A connection error ocurred.
    requests.exceptions.ConnectTimeout,   # The request timed out
    requests.exceptions.HTTPError,        # An HTTP error occurred
    requests.exceptions.ReadTimeout,      # The server did not send data in time
)

logger = logging.getLogger(__name__)


class MediaNamesCache(object):
    VIEW_URL = 'https://welo.tv/channels/channel/{}'

    def __init__(self, filename, invalidation_td=None):
        self.invalidation_td = invalidation_td or timedelta(days=10)
        try:
            self.data = shelve.open(filename + '.shelve', writeback=True)
        except Exception as e:
            self.data = shelve.open(filename + '2.shelve', writeback=True)

    def get_identifier_for(self, mediadelivery_id):
        key = str(mediadelivery_id)
        name, date_added = self.data.get(key, (None, None))
        if date_added is None or (datetime.now() - date_added) > self.invalidation_td:
            self.update_data(mediadelivery_id)

        name, date_added = self.data.get(key, (None, None))
        return name

    def update_data(self, mediadelivery_id):

        key = str(mediadelivery_id)
        url = self.VIEW_URL.format(mediadelivery_id)
        logger.info('Cache miss. Requesting media info at "{}"'.format(url))

        try:
            response = requests.get(url)
            name = response.json()['name']
        except requests_exceptions as e:
            logger.exception(e)
        except (TypeError, ValueError) as e:
            logger.exception(e)
        else:
            dt = datetime.now()
            self.data[key] = (name, dt)
            logger.info(u'Updated cache ({}, {}, {})'.format(mediadelivery_id, name, dt))
            self.data.sync()

    def __del__(self):
        try:
            self.data.close()
        except AttributeError:
            pass
