Usage of generic_metric.py
==========================

Configuration
-------------

To use this module it is necessary to define the INFLUXDB_SETTINGS dictionary
with the correct settings for `db_url` (InfluxDB server url), `db_name`
(database name), `db_user` (username), `db_pass` (password). If this dictionary
is added to the Django project settings the module will use it automatically.

```
INFLUXDB_SETTINGS = {
    'db_url': 'http://server.url',
    'db_name': 'name',
    'db_user': 'user',
    'db_pass': 'password'
}
```

Using the GenericMetric class directly
--------------------------------------

The GenericMetric class can be used directly providing the metric name, a
dictionary of dimensions, a dictionary of values and a UTC datetime object as
initial parameters.

```
from datetime import datetime
from generic_metric inport GenericMetric


dimesions = {'dim name 1': 'value', 'dim name 2': 'value'}
values = {'value name 1': value, 'value name 2': value}
GenericMetric('metric_name', dimensions, values, datetime.utcnow()).post_data()
```

Subclassing GenericMetric
-------------------------

Alternatively, GenericMetric can be subclassed to have a simpler interface for
frequently used metrics.

For example, to post the duration of a process a ProcessDurationMetric class
can be created that overrides the `__init__` method of GenericMetric and sets
everything up.

```
from datetime import datetime
from generic_metric import GenericMetric


class ProcessDurationMetric(GenericMetric):

    def __init__(self, process_name, process_duration):
        self.metric = 'process_duration'
        self.dimensions = {'name': process_name}
        self.values = {'duration': process_duration}
        self.timestamp = datetime.utcnow()


ProcessDurationMetric('some_process', 5.8).post_data()
```
