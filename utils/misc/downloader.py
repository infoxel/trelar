from shutil import copyfileobj
from urllib.request import urlopen


def download_file(source_url, destination_path, timeout_seconds):
    with urlopen(source_url, None, timeout_seconds) as stream:
        with open(destination_path, 'wb') as file_:
            copyfileobj(stream, file_)
