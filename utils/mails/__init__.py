# -*- coding: utf-8 -*-
"""
Classes to send mails
"""

import logging
import logging.config
import os
import smtplib
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from .config import LOGGING_CONF

logging.config.dictConfig(LOGGING_CONF)

logger = logging.getLogger(__name__)


class Mail(object):
    def __init__(self, config, subject, msg):
        self.account = config['account']
        self.passwd = config['passwd']
        self.server = config['server']
        self.to = config['to']
        self.subject = subject
        self.msg = msg.encode('utf-8')

    @property
    def username(self):
        return self.account.split('@')[0]

    @property
    def content(self):
        headers = [
            'from: {}'.format(self.account),
            'to: {}'.format(','.join(self.to)),
            'subject: {}'.format(self.subject),
            'content_type : text/html'
        ]
        return '\r\n\r\n'.join(headers + self.msg)

    def send(self, timeout=30):
        logger.info('Sending email using timeout %d', timeout)
        try:
            server = smtplib.SMTP(self.server, timeout=timeout)
            server.ehlo()
            server.starttls()
            server.login(self.account, self.passwd)
            server.sendmail(self.account, self.to, self.content)
            server.quit()
        except smtplib.SMTPException as e:
            logger.exception(e)


class HTMLMail(Mail):
    """Send mail with html markup"""
    def __init__(self, config, subject, msg):
        super(HTMLMail, self).__init__(config, subject, msg)

        self.msg = MIMEMultipart('alternative')
        self.msg['Subject'] = self.subject
        self.msg['From'] = self.account
        self.msg['To'] = ','.join(self.to)
        self.msg.attach(MIMEText(msg, 'html', 'utf-8'))

    @property
    def content(self):
        return self.msg.as_string()


def mail_factory(config, klass):

    class PreConfiguredMailSender(klass):
        def __init__(self, subject, msg):
            super(PreConfiguredMailSender, self).__init__(config, subject, msg)

    return PreConfiguredMailSender


def crontab_error_mail_factory(server_name, config, logfile):

    failing_script = os.path.splitext(os.path.basename(logfile))[0]

    with open(logfile, 'r') as log:
        error = ''.join(log.readlines()[-50:])

    subject = '{} - Error al ejecutar {}'.format(server_name, failing_script)

    msg_tpl = ('<h2>{failing_script}:</h2>'
               '<br/>'
               '{when:%Y-%m-%d %H:%M}'
               '<br/>'
               '<pre>{error}</pre>')

    msg = msg_tpl.format(failing_script=failing_script,
                         when=datetime.now(),
                         error=error)

    cls = mail_factory(config, HTMLMail)
    mail_sender = cls(subject, msg)
    return mail_sender
