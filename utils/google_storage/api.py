from google.cloud import storage
from os.path import basename


class GoogleStorage(object):
    def __init__(self, bucket_name):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(bucket_name)

    def upload_file(self, filename, storage_path=None, prefix=None):
        storage_filename = basename(filename)

        if storage_path is not None:
            storage_filename = storage_path

        blob = self.bucket.blob(storage_filename)
        if not blob.exists():
            blob.upload_from_filename(filename)
        return blob

    def delete_file(self, storage_path):
        blob = self.bucket.blob(storage_path)
        blob.delete()

    def download_file(self, storage_path, filename=None):
        blob = self.bucket.blob(storage_path)
        dest = basename(storage_path)
        if filename is not None:
            dest = filename

        blob.download_to_filename(dest)

    def list_blobs(self, **kwargs):
        return self.bucket.list_blobs(**kwargs)


class OriginStorage(GoogleStorage):
    def __init__(self):
        super(OriginStorage, self).__init__('origin-storage')
